<?php
include'init.php';

// Controleur qui permet l'affichage des notifications des messages non lus (appel de la page en ajax dans index.js)
if (!($_SESSION['currentUser']->getId_Message())) {
  echo  Message::getNombreMessage();
}else{
  echo User::getNombreDeMessageNonLu($_SESSION['currentUser']->getId());
}
