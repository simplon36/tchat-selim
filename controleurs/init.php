<?php
  function chargerClasse ($classe){
    if (file_exists('../Classes/'.$classe . '.php')) {
      require '../Classes/'.$classe . '.php'; // On inclue la classe correspondante au paramètre passé
    } else if(file_exists('../Repo/'.$classe . '.php')){
      require '../Repo/'.$classe . '.php';
    } else {
      exit("Le fichier $classe.php n'existe ni dans Classes ni dans Repo.");
    }

  }

spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée

session_start();


