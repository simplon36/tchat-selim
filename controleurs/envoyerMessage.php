<?php
include'init.php';
// Controleur qui permet l'envoie d'un message (appel de la page en ajax dans index.js)
if (isset($_POST) && isset($_SESSION['currentUser'])) {
      $date = date_default_timezone_set('Europe/Paris');
      $date = date('Y-m-d H:i:s');
      $message = $_POST['message'];
      $idUser = $_SESSION['currentUser']->getId();
      $posterMessage = new MessageRepo();
      $result = $posterMessage->createMessage($message,$date,$idUser);
}
