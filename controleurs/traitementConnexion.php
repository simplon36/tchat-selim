<?php

// Controleur qui permet la connexion de l'utilisateur tout en effectuant des verifications
if (isset($_POST['mail']) && isset($_POST['password'])) {
  $mail = $_POST['mail'];
  $password = $_POST['password'];

  $data = ['mail'=>$mail];
  $user = new User($data);
  $verifMail = $user->selectUser($mail);

  if ($verifMail == "ce mail n'existe pas"){
    $alerte = "Ce mail n'existe pas.";
  }else{
    $mail = $user->getMail();
    if (password_verify($password, $user->getMdp())) {
      $_SESSION['currentUser'] = $user;
      header('Location:index.php');
    }else{
      $alerte = 'Mot de passe incorrect.';
    }
  }

}
