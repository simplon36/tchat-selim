<?php
// Controleur qui permet l'inscription de l'utilisateur tout en effectuant des verifications(notament le hash du mdp)
if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['password'])) {

    // Hash du mot de passe.
    $mdp = password_hash($_POST['password'], PASSWORD_DEFAULT);

    //infos permettant la création de l'utilisateur
    $data = [
        'nom' => htmlspecialchars($_POST['nom']),
        'prenom'=>htmlspecialchars($_POST['prenom']),
        'pseudo'=> htmlspecialchars($_POST['pseudo']),
        'mail'=> htmlspecialchars($_POST['mail']),
        'mdp'=> $mdp];
    $user = new User($data);

    // Création du nouvel utilisateur en faisant appel à la méthode createUser de la class UserRepo
    $alerte = $user->createUser($_POST['pseudo'],$_POST['nom'],$_POST['prenom'],$_POST['mail'],$mdp);
}
