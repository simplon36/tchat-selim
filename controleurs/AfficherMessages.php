<?php
include'init.php';

$result = Message::getAllMessages();

// Controleur qui permet l'affichage de tout les messages (appel de la page en ajax dans index.js)
$IdDernierMessage;
$row=0;
while (isset($result[$row])) {
  if ($result[$row]['id_user'] == $_SESSION['currentUser']->getId()) {

     echo '<div class="me">
    <div class="info">
      <p>Vous '.substr($result[$row]['date_message'],5,6).'</p>
      <p>'.substr($result[$row]['date_message'],10).'</p>
    </div>
    <p class="contenu">
      '.$result[$row]['contenu_message'].'
    </p>
  </div>';

  $IdDernierMessage = $result[$row]['id_message'];
  }else{
   $others = User::getUserById($result[$row]['id_user']);

   echo ' <div class="notme">
            <div class="info" id="'.$result[$row]['id_message'].'">
              <p>'.$others['pseudo_user'].' '.substr($result[$row]['date_message'],5,6).' </p>
              <p>'.substr($result[$row]['date_message'],10).'</p>
            </div>
            <p class="contenu">
              '.$result[$row]['contenu_message'].'
            </p>
          </div>';

    $IdDernierMessage = $result[$row]['id_message'];
  }

$row++;

}
$_SESSION['currentUser']->setId_message($IdDernierMessage);

