<!-- NAVBAR LATERAL -->
<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/navLateral.css">
  <title>LeChat</title>
</head>
<body>
  <div id="main">

    <div id="logo">
      <img id="img" src="img/Logo.png" alt="">
      <h1 id="title">LE CHAT</h1>
    </div>

      <div id="user">
        <?php
        // En fonction de si l'utilisateur est connecté ou non, liens de connexion ou de déconnexion
          if (isset($_SESSION['currentUser'])) { ?>
            <a class="connexionDeco" id="deconnexion" href="../controleurs/deconnexion.php">Deconnexion</a>
            <?php
              }else{
                ?>
                <a class="connexionDeco" id="connexion" href="connexion.php">Connexion</a>
              <?php
            }
          ?>
      </div>
  </div>
</body>
</html>
