<?php
include '../controleurs/init.php';
include 'navLateral.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/index.css">
  <title>LeChat</title>
</head>
<body>

  <?php
  // Affichage seulement si un utilisateur est connecté
  if (isset($_SESSION['currentUser'])) { ?>
    <!-- Boutons permettant d'afficher la conversation ou d'en sortir, différent appel ajax se lance ou s'arrêtent en fonction du bouton clicker -->
    <div id="divButtonStart"style="display:flex; justify-content:center; padding-top:40vh;">
      <div id="buttonStart">
        <p id="textNotif">Vous avez <span id="nbNotif"></span> messages non lus.</p>
        <button id="startButton" onclick="start()">Rejoindre la discussion</button>
      </div>
    </div>
      <div id="buttonStop">
          <button id="stopButton" onclick="stop()"><img src="img/croix.png" alt=""></button>
        </div>
    <div id="messages">

    </div>
    <!-- Input envoie des messages, appel ajax (onclick) -->
    <div id="inputs">
        <textarea class="sendMessage" id="Message" name="message" required placeholder=" Votre Message.." autocomplete="off" cols="60" rows="2"></textarea>
        <input id="submit" type="button" value ="Envoyer" onclick="envoyerMessage()">
    </div>
 <?php }else{ ?>
  <!-- Affichage lorsqu'un utilisateur n'est pas connecté -->
  <div id="authentification">
    <h1 id="pasConnecter">Veuillez vous authentifier pour accéder au T’chat !</h1>
    <a href="connexion.php">Connexion</a>
  </div>
 <?php }

  ?>
      <script src="JS/index.js"></script>
</body>
</html>
