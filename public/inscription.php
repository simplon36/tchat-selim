<!-- PAGE D'INSCRIPTION -->
<?php
include'../controleurs/init.php';
include '../controleurs/traitementInscription.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/inscription.css">
  <title>Inscription</title>
</head>
<body>
    <!-- Formulaire d'inscription-->
<div id="main">

<div id="form">
      <h1 id="title">Inscription :</h1>
      <form id="inputs" action="#" method="post">
        <div class="inputLabel">
            <label for="nom">Nom :</label>
            <input id="nom" name="nom" type="text" placeholder=" nom" required>
        </div>
        <div class="inputLabel">
            <label for="prenom">Prénom :</label>
            <input id="prenom" name="prenom" type="text" placeholder=" prénom" required>
        </div>
        <div class="inputLabel">
            <label for="pseudo">Pseudo :</label>
            <input id="pseudo" name="pseudo" type="text" placeholder=" pseudo" required>
        </div>
        <div class="inputLabel">
            <label for="mail">Mail :</label>
            <input id="mail" name="mail" type="mail" placeholder=" mail" required>
        </div>
        <div class="inputLabel">
            <label for="password">Mot de passe :</label>
            <input id="password" name="password" type="password" placeholder=" mot de passe" required>
        </div>
        <input id="submit" type="submit" value="Finalisez l'inscription">

      </form>
<?php

if (isset($alerte)){
  echo '<p style="color:red; text-align:center;">'.$alerte.'</p>';
};


?>
</div>
</body>
</html>
