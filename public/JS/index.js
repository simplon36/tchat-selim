
// On récupère toutes les classes ou id nécessaires.
let startButton = document.getElementById('startButton');
let stopButton = document.getElementById('stopButton');
let messages = document.getElementById('messages');
let inputMessage = document.getElementById('Message');
let submit = document.getElementById('submit');
let nbNotif = document.getElementById('nbNotif');
let divButtonStart = document.getElementById('divButtonStart');

// Permet de lancer l'appel ajax pour les notifs au lancement de la page
startNotif = setInterval(notif,500);



// Function qui lance l'affichage des messages et stop celui des notifs
function start(){
  console.log(messages.style.display)
  if (messages.style.display!='flex') {
      startAffiche = setInterval(afficheMessage, 500);
      clearInterval(startNotif);
      stopButton.style.display = 'flex';
      divButtonStart.style.display = 'none';
      messages.style.display = 'flex';
      inputMessage.style.display = 'flex';
      submit.style.display = 'flex';
  }
}
// Function stoppant l'affichage des messages et lançant celui des notifs
function stop(){
  if (messages.style.display == 'flex') {
    startNotif = setInterval(notif,500);
    clearInterval(startAffiche);
    divButtonStart.style.display = 'flex';
    stopButton.style.display = 'none';
    messages.style.display = 'none';
    inputMessage.style.display = 'none';
    submit.style.display = 'none';


  }
}

//Appel ajax des notifications
function notif(){
    let xml = new XMLHttpRequest();
    xml.open('POST','../controleurs/notif.php', true);
    xml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    xml.send();
    xml.onreadystatechange = function(){
      if ((xml.readyState === 4) && (xml.status === 200)) {

          nbNotif.innerHTML = xml.responseText;
     }
  }
}

// Ces lignes de codes arrêtent temporairement l'appel ajax des messages,
// si la barre de scroll bouge(à améliorer).
userHasScrolled = false;
messages.onscroll = function (e)
{
    userHasScrolled = true;
}

//Appel ajax des messages
function afficheMessage(){
      if (userHasScrolled==true) {
        userHasScrolled = false;
      }else{
            let messages = document.getElementById('messages');
            let xml = new XMLHttpRequest();
            xml.open('POST','../controleurs/AfficherMessages.php', true);
            xml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
            xml.send();
            var messageBody = document.querySelector('#messages');
            messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
            xml.onreadystatechange = function(){
              if ((xml.readyState === 4) && (xml.status === 200)) {

                    messages.innerHTML = xml.responseText;
            }
          }
        }
  }

// Appel ajax de l'envoie des messages
 function envoyerMessage(){
  let message = document.getElementById('Message');
  let xml = new XMLHttpRequest();
  xml.open('POST','../controleurs/envoyerMessage.php', true);
  xml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  $requete = 'message='+message.value;
  xml.send($requete);
  message.value="";
};
