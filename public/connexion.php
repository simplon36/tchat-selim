<!-- PAGE DE CONNEXION -->
<?php
include'../controleurs/init.php';
include'../controleurs/traitementConnexion.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/connexion.css">
  <title>Connexion</title>
</head>
<body>
  <div id="main">
      <h1 id="title" style="text-align:center;">Connexion</h1>

      <!-- Formulaire de connexion -->
      <form id="form" action="#" method="post">
        <label for="email">Mail : <input id="email" type="mail" name="mail" required placeholder=" mail"></label>
        <label for="mdp">Mot de passe : <input id="mdp" type="password" name="password" required placeholder=" mot de passe"></label>
        <input id="submit" type="submit" value="Connectez-vous">
      </form>
      <a id="inscription" href="inscription.php">Pas encore inscris ?</a>

      <?php
    // Alerte en cas de mauvaise tentative de connexion
    if (isset($alerte)){
      echo '<p style="color:red; text-align:center;">'.$alerte.'</p>';
    };

?>
</div>
</body>
</html>
