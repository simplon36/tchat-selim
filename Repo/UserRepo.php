<?php

class UserRepo{

  private
  $_db,
  $_pseudo,
  $_nom,
  $_prenom,
  $_mail,
  $_mdp;


  public function __construct(){
    $this->_db = new DataBase();
    $this->_db = $this->_db->getDataBase();
  }

  // Methode permettant la création d'un utilisateur dans la base de données.
  public function createUser($pseudo,$nom,$prenom,$mail,$mdp){
        // Si toutes les conditions sont réunis alors on crée l'utilisateur dans la base de données.
        $this->_pseudo = $pseudo;
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_mail = $mail;
        $this->_mdp = $mdp;
        $data = [
                ':pseudo' => $this->_pseudo,
                ':nom' => $this->_nom,
                ':prenom' => $this->_prenom,
                ':mail' => $this->_mail,
                ':mdp' => $this->_mdp];
        $sql = 'INSERT INTO users (pseudo_user,nom_user,prenom_user,mail_user,mdp_user) VALUES (:pseudo, :nom, :prenom, :mail, :mdp) ';
        $requete = $this->_db->prepare($sql);
        $requete->execute($data);

        $this->_db = null;

        header('Location:connexion.php');
        exit;
  }


  //Methode permettant de récupérer un utilisateur grâce à son Id
  public function getUser(int $id){

    $sql = 'SELECT * FROM users WHERE id_user = '.$id.'';
    $requete = $this->_db->query($sql);
    return $requete->fetch();
    $this->_db = null;
  }

  //Methode permettant de récupérer/vérifié un pseudo dans la base de données. (ici on s'en sert pour vérifier si un pseudo existe déjà lors de l'inscription de l'utilisateur)
  public function getPseudo(string $pseudo){

      $sql = 'SELECT pseudo_user FROM users WHERE pseudo_user = :pseudo';
      $requetePseudo = $this->_db->prepare($sql);
      $requetePseudo->execute([':pseudo'=>$pseudo]);
      $resultPseudo = $requetePseudo->fetch();

      return $resultPseudo;
  }

  //Methode permettant de récupérer/vérifié un mail dans la base de données. (ici on s'en sert pour vérifier si un mail existe déjà lors de l'inscription de l'utilisateur)
  public function getMail(string $mail){

      $sql = 'SELECT mail_user FROM users WHERE mail_user = :mail';
      $requeteMail = $this->_db->prepare($sql);
      $requeteMail->execute([':mail'=>$mail]);
      $resultMail = $requeteMail->fetch();

      return $resultMail;
  }

  //Methode permettant de récupérer un utilisateur grâce à son mail.
  public function getUserByMail(string $mail){
    $sql = 'SELECT * FROM users WHERE mail_user = :mail_user';
    $requete = $this->_db->prepare($sql);
    $requete->execute([':mail_user'=>$mail]);
    $result = $requete->fetch();

    return $result;
  }

  //Methode permettant d'inscrire dans la base de données l'id du dernier messages lu par l'utilisateur.
  public function setDernierMessageLu(int $id, int $dernierMessageLu){
    $sql = 'UPDATE users SET id_message = :dernierMessageLu WHERE id_user = :id';
    $data = [
              ':id'=>$id,
              ':dernierMessageLu'=>$dernierMessageLu];
    $requete = $this->_db->prepare($sql);
    $requete->execute($data);
    return $requete->fetch();
    $this->_db = null;
  }

  //Methode permettant de récupérer le nombre de Non lu par l'utilisateur
  public function getNombreDeMessageNonLu($id){
    $sql = "SELECT COUNT(*) FROM users, messages WHERE users.id_user = :id AND messages.id_message BETWEEN users.id_message AND messages.id_message ";
    $requete = $this->_db->prepare($sql);
    $requete->execute([':id'=>$id]);
    return $requete->fetch();
  }
}
