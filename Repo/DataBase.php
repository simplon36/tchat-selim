<?php

class DataBase{
  // Informations relatives à la connexion de la base de données
  CONST HOST = "localhost";
  CONST DB_NAME = "lechat";
  CONST USER_DB = "LeChat";
  CONST USER_MDP = "1234";
  CONST DB_TABLE = "messages";

  private $_db;

  public function __construct(){

    $this->connexionDB();
  }


  // Connexion à la base de données
  private function connexionDB(){
    try {

      $this->_db = new PDO('mysql:host='.self::HOST.';dbname='.self::DB_NAME.';',self::USER_DB,self::USER_MDP);
      $this->_db->query('SET CHARACTER SET utf8'); // Pour ne pas avoir de problème d'accent
    } catch (PDOExeption $e) {
      echo "Connexion échoué car : ".$e->getMessage();
    }

  }


  public function getDataBase(){
    return $this->_db;
  }

   public function initialisationDB(){
    $verif = $this->_db->query("SHOW TABLES LIKE '".self::DB_TABLE ."'");
    $verif = $verif->fetchAll(PDO::FETCH_ASSOC);
      if (empty($verif) || in_array(self::DB_TABLE,$verif[0])){
        $sql = "CREATE TABLE IF NOT EXISTS `messages` (
            'id_user'     Int  Auto_increment  NOT NULL ,
            'pseudo_user' Varchar (100) NOT NULL ,
            'nom_user'    Varchar (100) NOT NULL ,
            'prenom_user' Varchar (100) NOT NULL ,
            'mail_user'   Varchar (100) NOT NULL ,
            'mdp_user'   Varchar (100) NOT NULL ,
            'id_message'  Int
            ,CONSTRAINT 'users_PK' PRIMARY KEY ('id_user')
          )ENGINE=InnoDB;

        CREATE TABLE IF NOT EXISTS `users` (
        'id_message'      Int  Auto_increment  NOT NULL ,
        'contenu_message' Mediumtext NOT NULL ,
        'date_message'   Datetime NOT NULL ,
        'like_message'    Int NOT NULL ,
        'id_user'         Int NOT NULL
          ,CONSTRAINT 'messages_PK' PRIMARY KEY ('id_message')
        )ENGINE=InnoDB;

        ALTER TABLE users
        ADD CONSTRAINT users_messages0_FK
        FOREIGN KEY (id_message)
        REFERENCES messages(id_message);

        ALTER TABLE messages
        ADD CONSTRAINT messages_users0_FK
        FOREIGN KEY (id_user)
        REFERENCES users(id_user);";

        $this->_db->query($sql);

        echo "La table ".self::DB_TABLE." a été créée.";

      }
  }


}
