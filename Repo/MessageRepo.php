<?php

class MessageRepo{
  public $contenu ="",
  $dateDuMessage ="",
  $id_user = null,
  $id_message = null;

  private $_db;

  public function __construct(){

    $this->_db = new DataBase();
    $this->_db = $this->_db->getDataBase();

  }
// Methode permettant la création d'un message dans la base de données.
  public function createMessage(string $contenu, $date, $id_user){

        $this->contenu = $contenu;
        $this->dateDuMessage = $date;
        $this->id_user = $id_user;
        $sql = 'INSERT INTO messages (contenu_message,date_message,id_user) VALUES (?,?,?) ';
        $requete = $this->_db->prepare($sql);
        $requete->execute([$this->contenu,$this->dateDuMessage,$this->id_user]);
      $this->_db = null;

  }
  //Methode pour le moment inactive, prévu pour ajouter une feature Supprimer aux messages
  public function supprMessage(int $id_message){

      $this->id_message = $id_message;

      $data = [':id_message' => $this->id_message];

      $sql ='DELETE * FROM messages WHERE id_message = :id_message';
      $requete = $this->_db->prepare($sql);
      $requete->execute($data);
    $this->_db = null;
  }
  // Methode pour le moment inactive, prévu pour ajouter une feature Like aux messages
  public function likeMessage(){
    $sql = 'UPDATE messages SET like_message = like_message+1';
    $requete = $this->_db->query($sql);
    $this->_db = null;
  }

   // Methode permettant de récupérer tous les messages inscrits dans la base de données
  public function getAllMessages(){
    $sql = 'SELECT * FROM messages';
    $requete = $this->_db->query($sql);
    $result = $requete->fetchAll(PDO::FETCH_ASSOC);

    $this->_db = null;

    return $result;

  }
  // Methode permettant de récupérer le nombre total de messages inscrits dans la base de données
  public function getNbMessages(){
    $sql = 'SELECT COUNT(*) from messages';
    $requete = $this->_db->query($sql);
    $result = $requete->fetch();

    $this->_db = null;

    return $result;
  }
}
