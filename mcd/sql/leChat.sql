#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: users
#------------------------------------------------------------

CREATE TABLE users(
        id_user     Int  Auto_increment  NOT NULL ,
        pseudo_user Varchar (100) NOT NULL ,
        nom_user    Varchar (100) NOT NULL ,
        prenom_user Varchar (100) NOT NULL ,
        mail_user   Varchar (100) NOT NULL ,
        mdp_user    Varchar (100) NOT NULL ,
        id_message  Int
	,CONSTRAINT users_PK PRIMARY KEY (id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: messages
#------------------------------------------------------------

CREATE TABLE messages(
        id_message      Int  Auto_increment  NOT NULL ,
        contenu_message Mediumtext NOT NULL ,
        date_message    Datetime NOT NULL ,
        like_message    Int NOT NULL ,
        id_user         Int NOT NULL
	,CONSTRAINT messages_PK PRIMARY KEY (id_message)
)ENGINE=InnoDB;




ALTER TABLE users
	ADD CONSTRAINT users_messages0_FK
	FOREIGN KEY (id_message)
	REFERENCES messages(id_message);

ALTER TABLE messages
	ADD CONSTRAINT messages_users0_FK
	FOREIGN KEY (id_user)
	REFERENCES users(id_user);
