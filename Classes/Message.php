<?php

class Message{
private
  $_id,
  $_contenu,
  $_date,
  $_like,
  $_id_user;

  public function __construct(array $infos){
    $this->hydrate($infos);
  }

  /**
   * [hydrate : permet de construire l'objet de façon automatiser en allant chercher les setters]
   * @param  array  $infos [un tableau regroupant toutes les variables pour construire un utilisateur]
   * @return [nothing]
   */
  private function hydrate(array $infos){
    foreach ($infos as $key => $value) {
      $method = 'set'.ucfirst($key);
      if (method_exists($this, $method)) {
        $this->$methode($value);
      }
    }
  }

  // SETTERS
  private function setId(int $id){
    $this->_id = $id;
  }
  private function setContenu(string $contenu){
    $this->_contenu = $contenu;
  }
  private function setDate(string $date){
    $this->_date = $date;
  }
  private function setLike(string $like){
    $this->_like = $like;
  }
  private function setId_user(int $id_user){
    $this->_id_user = $id_user;
  }

  // GETTERS
  public function getId(){
    return $this->_id;
  }
  public function getContenu(){
    return $this->_contenu;
  }
  public function getDate(){
    return $this->_date;
  }
  public function getLike(){
    return $this->_like;
  }
  public function getId_user(){
    return $this->_id_user;
  }

  //Methode permettant de récupérer tous les messages inscrits dans la base de données en faisant appel au repository : Message
  public static function getAllMessages(){
    $messages = new MessageRepo();
    $result = $messages->getAllMessages();
    return $result;
  }
  //Methode permettant de récupérer le nombre de messages total inscrits dans la base de données en faisant appel au repository : Message
  public static function getNombreMessage(){
    $messages = new MessageRepo();
    $result = $messages->getNbMessages();
    return $result[0];
  }
}
