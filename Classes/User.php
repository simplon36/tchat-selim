<?php
class User{
  private
  $_id,
  $_pseudo,
  $_nom,
  $_prenom,
  $_mail,
  $_mdp,
  $_dernierMessageLu;

  public function __construct(array $infos){
    $this->hydrate($infos);
  }

  /**
   * [hydrate : permet de construire l'objet de façon automatiser en allant chercher les setters]
   * @param  array  $infos [un tableau regroupant toutes les variables pour construire un utilisateur]
   * @return [nothing]
   */
  private function hydrate(array $infos){
    foreach ($infos as $key => $value) {
      $method = 'set'.ucfirst($key);
      if (method_exists($this, $method)) {
        $this->$method($value);
      }
    }
  }

  // SETTERS
  private function setId(int $id){
    $this->_id = $id;
  }
  private function setNom(string $nom){
    $this->_nom = $nom;
  }
  private function setPrenom(string $prenom){
    $this->_prenom = $prenom;
  }
  private function setPseudo(string $pseudo){
    $this->_pseudo = $pseudo;
  }
  private function setMail(string $mail){
    $this->_mail = $mail;
  }
  private function setMdp(string $mdp){
    $this->_mdp = $mdp;
  }
  public function setId_message(int $dernierMessageLu){
    $this->_dernierMessageLu = $dernierMessageLu;
    $user = new UserRepo;
    $user->setDernierMessageLu($this->getId(),$dernierMessageLu);
  }

  // GETTERS
  public function getId(){
    return $this->_id;
  }
  public function getNom(){
    return $this->_nom;
  }
  public function getPrenom(){
    return $this->_prenom;
  }
  public function getPseudo(){
    return $this->_pseudo;
  }
  public function getMail(){
    return $this->_mail;
  }
  public function getMdp(){
    return $this->_mdp;
  }
  public function getId_Message(){
    return $this->_dernierMessageLu;
  }

  // Methode permettant la creation d'un utilisateur, tout en appliquant diverses vérifications lors de son inscription. Cette methode utilise le repository : User
  public function createUser(){


    $resultMail = new UserRepo;
    $resultMail = $resultMail->getMail($this->getMail());
    $resultPseudo = new UserRepo();
    $resultPseudo = $resultPseudo->getPseudo($this->getPseudo());

    if ($resultPseudo || $resultMail) {
      /* Si le Pseudo ou le Mail existe déjà dans la DataBase alors impossible de
      s'inscrire et alerte envoyé à l'utilisateur*/
        $alerte = 'Mail ou pseudo déjà pris.';
        return $alerte;

    } else if(!filter_var($this->getMail(), FILTER_VALIDATE_EMAIL)) {
      /* Si le mail reçu est invalide (vérifié par FILTER_VALIDE_EMAIL) (exemple : pas de @)
      alors impossible de s'inscrire et alerte envoyé à l'utilisateur*/
        $alerte = 'Mail incorrect.';
        return $alerte;
    }else{
      $createUser = new UserRepo();
      $createUser->createUser($this->getPseudo(),$this->getNom(),$this->getPrenom(),$this->getMail(),$this->getMdp(),$this->getId_Message());
    }

  }
  // Methode permettant de selectionner un utilisateur grâce à son mail en faisant appel au repository : User
  public function selectUser(string $mail){
    $user = new UserRepo();
    $user = $user->getUserByMail($mail);

    if (isset($user['id_user'])) {
      $this->_id = $user['id_user'];
      $this->_pseudo = $user['pseudo_user'];
      $this->_nom = $user['nom_user'];
      $this->_prenom = $user['prenom_user'];
      $this->_mail = $user['mail_user'];
      $this->_mdp = $user['mdp_user'];
      $this->_dernierMessageLu = $user['id_message'];
      }else{
        $alerte = "ce mail n'existe pas";
        return $alerte;
      }


  }
  //Methode permettant de récupérer un utilisateur grâce à son Id en faisant appel au repository : User
  public static function getUserById(int $id){
    $user = new UserRepo;
    $result = $user->getUser($id);
    return $result;
  }

 //Methode permettant de récupérer le nombre de messages non lu par l'utilisateur en faisant appel au repository : User
  public static function getNombreDeMessageNonLu(int $id){
    $user = new UserRepo;
    $result = $user->getNombreDeMessageNonLu($id);
    return (int)$result[0]-1;
  }
}
